﻿namespace Shifr_dEs
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.RADIO_BUTTON_DECODER = new System.Windows.Forms.RadioButton();
            this.RADIO_BUTTON_SHIFR = new System.Windows.Forms.RadioButton();
            this.SAVE_SHIFR_TEXT = new System.Windows.Forms.Button();
            this.BUTTON_SHIFR_TEXT = new System.Windows.Forms.Button();
            this.SHIFR_TEXT_FROM_WINDOW = new System.Windows.Forms.TextBox();
            this.BUTTON_TEXT_FROM_COMP = new System.Windows.Forms.Button();
            this.GEN_KEY = new System.Windows.Forms.Button();
            this.KEY_BOX = new System.Windows.Forms.TextBox();
            this.TEXT_FROM_WINDOW = new System.Windows.Forms.TextBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.Control;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label3.Location = new System.Drawing.Point(434, 387);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 17);
            this.label3.TabIndex = 38;
            this.label3.Text = "←   →";
            // 
            // RADIO_BUTTON_DECODER
            // 
            this.RADIO_BUTTON_DECODER.AutoSize = true;
            this.RADIO_BUTTON_DECODER.BackColor = System.Drawing.SystemColors.Control;
            this.RADIO_BUTTON_DECODER.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.RADIO_BUTTON_DECODER.Location = new System.Drawing.Point(503, 387);
            this.RADIO_BUTTON_DECODER.Name = "RADIO_BUTTON_DECODER";
            this.RADIO_BUTTON_DECODER.Size = new System.Drawing.Size(115, 19);
            this.RADIO_BUTTON_DECODER.TabIndex = 35;
            this.RADIO_BUTTON_DECODER.TabStop = true;
            this.RADIO_BUTTON_DECODER.Text = "Расшифровать";
            this.RADIO_BUTTON_DECODER.UseVisualStyleBackColor = false;
            this.RADIO_BUTTON_DECODER.CheckedChanged += new System.EventHandler(this.RADIO_BUTTON_DECODER_CheckedChanged);
            // 
            // RADIO_BUTTON_SHIFR
            // 
            this.RADIO_BUTTON_SHIFR.AutoSize = true;
            this.RADIO_BUTTON_SHIFR.BackColor = System.Drawing.SystemColors.Control;
            this.RADIO_BUTTON_SHIFR.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.RADIO_BUTTON_SHIFR.Location = new System.Drawing.Point(307, 387);
            this.RADIO_BUTTON_SHIFR.Name = "RADIO_BUTTON_SHIFR";
            this.RADIO_BUTTON_SHIFR.Size = new System.Drawing.Size(109, 19);
            this.RADIO_BUTTON_SHIFR.TabIndex = 34;
            this.RADIO_BUTTON_SHIFR.TabStop = true;
            this.RADIO_BUTTON_SHIFR.Text = "Зашифровать";
            this.RADIO_BUTTON_SHIFR.UseVisualStyleBackColor = false;
            this.RADIO_BUTTON_SHIFR.CheckedChanged += new System.EventHandler(this.RADIO_BUTTON_SHIFR_CheckedChanged);
            // 
            // SAVE_SHIFR_TEXT
            // 
            this.SAVE_SHIFR_TEXT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.SAVE_SHIFR_TEXT.Location = new System.Drawing.Point(617, 487);
            this.SAVE_SHIFR_TEXT.Name = "SAVE_SHIFR_TEXT";
            this.SAVE_SHIFR_TEXT.Size = new System.Drawing.Size(285, 45);
            this.SAVE_SHIFR_TEXT.TabIndex = 33;
            this.SAVE_SHIFR_TEXT.Text = "Сохранить зашифрованный текст";
            this.SAVE_SHIFR_TEXT.UseVisualStyleBackColor = true;
            this.SAVE_SHIFR_TEXT.Click += new System.EventHandler(this.SAVE_SHIFR_TEXT_Click);
            // 
            // BUTTON_SHIFR_TEXT
            // 
            this.BUTTON_SHIFR_TEXT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.BUTTON_SHIFR_TEXT.Location = new System.Drawing.Point(12, 440);
            this.BUTTON_SHIFR_TEXT.Name = "BUTTON_SHIFR_TEXT";
            this.BUTTON_SHIFR_TEXT.Size = new System.Drawing.Size(290, 41);
            this.BUTTON_SHIFR_TEXT.TabIndex = 32;
            this.BUTTON_SHIFR_TEXT.Text = "Зашифровать/Расшифровать текст";
            this.BUTTON_SHIFR_TEXT.UseVisualStyleBackColor = true;
            this.BUTTON_SHIFR_TEXT.Click += new System.EventHandler(this.BUTTON_SHIFR_TEXT_Click);
            // 
            // SHIFR_TEXT_FROM_WINDOW
            // 
            this.SHIFR_TEXT_FROM_WINDOW.Location = new System.Drawing.Point(617, 27);
            this.SHIFR_TEXT_FROM_WINDOW.Multiline = true;
            this.SHIFR_TEXT_FROM_WINDOW.Name = "SHIFR_TEXT_FROM_WINDOW";
            this.SHIFR_TEXT_FROM_WINDOW.Size = new System.Drawing.Size(285, 399);
            this.SHIFR_TEXT_FROM_WINDOW.TabIndex = 31;
            // 
            // BUTTON_TEXT_FROM_COMP
            // 
            this.BUTTON_TEXT_FROM_COMP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.BUTTON_TEXT_FROM_COMP.Location = new System.Drawing.Point(617, 431);
            this.BUTTON_TEXT_FROM_COMP.Margin = new System.Windows.Forms.Padding(2);
            this.BUTTON_TEXT_FROM_COMP.Name = "BUTTON_TEXT_FROM_COMP";
            this.BUTTON_TEXT_FROM_COMP.Size = new System.Drawing.Size(285, 41);
            this.BUTTON_TEXT_FROM_COMP.TabIndex = 29;
            this.BUTTON_TEXT_FROM_COMP.Text = "Загрузить текст";
            this.BUTTON_TEXT_FROM_COMP.UseVisualStyleBackColor = true;
            this.BUTTON_TEXT_FROM_COMP.Click += new System.EventHandler(this.BUTTON_TEXT_FROM_COMP_Click);
            // 
            // GEN_KEY
            // 
            this.GEN_KEY.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.GEN_KEY.Location = new System.Drawing.Point(465, 423);
            this.GEN_KEY.Margin = new System.Windows.Forms.Padding(2);
            this.GEN_KEY.Name = "GEN_KEY";
            this.GEN_KEY.Size = new System.Drawing.Size(137, 24);
            this.GEN_KEY.TabIndex = 28;
            this.GEN_KEY.Text = "Сгенерировать ";
            this.GEN_KEY.UseVisualStyleBackColor = true;
            this.GEN_KEY.Click += new System.EventHandler(this.GEN_KEY_Click);
            // 
            // KEY_BOX
            // 
            this.KEY_BOX.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.KEY_BOX.Location = new System.Drawing.Point(341, 425);
            this.KEY_BOX.Margin = new System.Windows.Forms.Padding(2);
            this.KEY_BOX.Name = "KEY_BOX";
            this.KEY_BOX.Size = new System.Drawing.Size(111, 23);
            this.KEY_BOX.TabIndex = 27;
            // 
            // TEXT_FROM_WINDOW
            // 
            this.TEXT_FROM_WINDOW.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.TEXT_FROM_WINDOW.Location = new System.Drawing.Point(11, 27);
            this.TEXT_FROM_WINDOW.Margin = new System.Windows.Forms.Padding(2);
            this.TEXT_FROM_WINDOW.Multiline = true;
            this.TEXT_FROM_WINDOW.Name = "TEXT_FROM_WINDOW";
            this.TEXT_FROM_WINDOW.Size = new System.Drawing.Size(291, 399);
            this.TEXT_FROM_WINDOW.TabIndex = 26;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(914, 542);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.RADIO_BUTTON_DECODER);
            this.Controls.Add(this.RADIO_BUTTON_SHIFR);
            this.Controls.Add(this.SAVE_SHIFR_TEXT);
            this.Controls.Add(this.BUTTON_SHIFR_TEXT);
            this.Controls.Add(this.SHIFR_TEXT_FROM_WINDOW);
            this.Controls.Add(this.BUTTON_TEXT_FROM_COMP);
            this.Controls.Add(this.GEN_KEY);
            this.Controls.Add(this.KEY_BOX);
            this.Controls.Add(this.TEXT_FROM_WINDOW);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton RADIO_BUTTON_DECODER;
        private System.Windows.Forms.RadioButton RADIO_BUTTON_SHIFR;
        private System.Windows.Forms.Button SAVE_SHIFR_TEXT;
        private System.Windows.Forms.Button BUTTON_SHIFR_TEXT;
        private System.Windows.Forms.TextBox SHIFR_TEXT_FROM_WINDOW;
        private System.Windows.Forms.Button BUTTON_TEXT_FROM_COMP;
        private System.Windows.Forms.Button GEN_KEY;
        private System.Windows.Forms.TextBox KEY_BOX;
        private System.Windows.Forms.TextBox TEXT_FROM_WINDOW;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

